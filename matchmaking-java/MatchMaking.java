public class MatchMaking {

	public String makeMatch(String[] namesWomen, String[] answersWomen,
			String[] namesMen, String[] answersMen, String queryWoman) {
		quickSort(namesWomen, answersWomen, 0, namesWomen.length);
		quickSort(namesMen, answersMen, 0, namesMen.length);

		boolean[] chosen = new boolean[namesMen.length];

		for (int i = 0; i < namesWomen.length; i++) {
			int best = -1;
			int bestIdx = -1;
			for (int j = 0; j < namesMen.length; j++) {
				if (!chosen[j]) {
					int m = matchSize(answersWomen[i], answersMen[j]);
					if (m > best) {
						best = m;
						bestIdx = j;
					}
				}
			}
			if (namesWomen[i].equals(queryWoman)) {
				return namesMen[bestIdx];
			}
			chosen[bestIdx] = true;
		}

		return null;
	}

	public int matchSize(String ans1, String ans2) {
		int count = 0;
		for (int i = 0; i < ans1.length(); i++) {
			if (ans1.charAt(i) == ans2.charAt(i)) {
				count++;
			}
		}
		return count;
	}

	public void quickSort(String[] names, String[] answers, int st, int end) {
		if (st >= end) {
			return;
		}
		int pivotIdx = (end + st) / 2;
		String pivot = names[pivotIdx];
		int sep = st;

		swap(names, pivotIdx, end - 1);
		swap(answers, pivotIdx, end - 1);

		for (int i = st; i < end - 1; i++) {
			if (names[i].compareTo(pivot) < 0) {
				swap(names, sep, i);
				swap(answers, sep++, i);
			}
		}
		swap(names, end - 1, sep);
		swap(answers, end - 1, sep);

		quickSort(names, answers, st, sep);
		quickSort(names, answers, sep + 1, end);
	}

	public void swap(String[] arr, int i, int j) {
		String temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}
