import java.util.HashSet;
import java.util.Set;

public class UserName {

	public String newMember(String[] existingNames, String newName) {
		Set<String> set = new HashSet<String>();
		for(String ex : existingNames) {
			set.add(ex);
		}
		if(!set.contains(newName)) {
			return newName;
		}
		int i = 1;
		while(set.contains(newName + i)) {
			i++;
		}
		return newName + i;
	}
}
